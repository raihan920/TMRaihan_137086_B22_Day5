<?php
class MyMainClass
{
    function MyFunction1()
    {
        echo "It is showing the name of the Function: "."<b>".__FUNCTION__."</b>";
    }
    function MyFunction2()
    {
        echo "It is showing the Method: "."<b>".__METHOD__."</b>";
    }
}
$obj=new MyMainClass();
$obj->MyFunction1();
echo "<br/>";
$obj->MyFunction2();
?>